
---

**WARNING!**: This is the *Old* source-code repository for MGFVisor3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/mgfvisor3/) located at https://sourceforge.net/p/lp-csic-uab/mgfvisor3/**  

---  
  

![https://lh4.googleusercontent.com/-cKb0MrEqwMg/UOrfKCOV9sI/AAAAAAAAAT4/Zl2LJfTAOx4/s800/mvisor60x65.png](https://lh4.googleusercontent.com/-cKb0MrEqwMg/UOrfKCOV9sI/AAAAAAAAAT4/Zl2LJfTAOx4/s800/mvisor60x65.png)


---

**WARNING!**: This is the *Old* source-code repository for MGFVisor3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/mgfvisor3/) located at https://sourceforge.net/p/lp-csic-uab/mgfvisor3/**  

---  
  

**Table Of Contents:**

[TOC]

#### Description

**`MgfVisor`** is a tool for viewing collections of spectra stored as [MGF](http://www.matrixscience.com/help/data_file_help.html#GEN) files.

![https://lh3.googleusercontent.com/-WfJdQcMk5Xc/UOrfKFDsnhI/AAAAAAAAAT8/sfqb1sX1H5w/s720/mvisor_main.png](https://lh3.googleusercontent.com/-WfJdQcMk5Xc/UOrfKFDsnhI/AAAAAAAAAT8/sfqb1sX1H5w/s720/mvisor_main.png)

#### Features

  * Reads [MGF](http://www.matrixscience.com/help/data_file_help.html#GEN) files whose header tags include TITLE and SCANS tags.
  * Spectrum viewer with ion labels.
  * Diverse spectrum data collected (peak number, parent ion, rt time).
  * Detection of peak pairs suggesting an aa residue.
  * Estimate molecular ion and bn-1 transition
  * Graph transitions.
  * Detects graph with paths longer than two transitions.

![https://lh6.googleusercontent.com/-iWQm_PdY5EU/UOrfKItP-kI/AAAAAAAAAUA/1ztDoRHnVMI/s640/transitions.png](https://lh6.googleusercontent.com/-iWQm_PdY5EU/UOrfKItP-kI/AAAAAAAAAUA/1ztDoRHnVMI/s640/transitions.png)

#### Installation

Tested in Windows XP 32 bits and Windows 7 64 bits. It also works in Unix-like systems as GNU/Linux.

##### Requirements

  * [Graphviz](http://www.graphviz.org/Download_windows.php): `MgfVisor` uses the Graphviz package to plot graphs (last version tested is 2.28.0).

##### From Installer

  1. Download `MgfVisor_x.y.z_setup.exe` windows installer from repository at the [MgfVisor project download page](https://bitbucket.org/lp-csic-uab/mgfvisor/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `MgfVisor` by double-clicking on the `MgfVisor` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install [Python](http://www.python.org/) and third party software indicated in [Dependencies](#markdown-header-source-dependencies), as required.
  1. Download `MgfVisor` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/mgfvisor/src).
  1. Download commons source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/commons/src).
  1. Copy the `MgfVisor` and `commons` folders in your Python `site-packages` or in another folder in the `python path`. If you only plan to run the program from a particular user, you will need only to put `MgfVisor` folder inside a user folder and then the `commons` folder inside `MgfVisor` folder.
  1. Run `MgfVisor` by double-clicking on the `mvisor.pyw` module, or typing `python mvisor.pyw` on the command line.

###### _Source Dependencies:_

  * [Python](http://www.python.org/) 2.7 (not tested with other versions)
  * [wxPython](http://www.wxpython.org/) 2.8.11.0 - 2.8.11.2
  * [matplotlib](https://github.com/matplotlib/matplotlib/downloads) 1.2.0
  * [pydot](http://code.google.com/p/pydot/) 1.0.28
  * [networkX](http://networkx.lanl.gov/) 1.7
  * [commons](https://bitbucket.org/lp-csic-uab/commons/src) (from LP CSIC/UAB BitBucket [repository](https://bitbucket.org/lp-csic-uab/commons/src))

Third-party program versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine. [FastaDBSetup.png]

#### Download

You can download the last version of **`MgfVisor`** [here](https://bitbucket.org/lp-csic-uab/mgfvisor/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/mgfvisor/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code

#### Change-Log

1.0.0 December 2012

  * First version release.

#### To-Do

  * Take into account the monocharged parent ion to mark its position in the spectrum_._

#### Contribute

These programs are made to be useful. If you use them and don't work entirely as you expect, let us know about features you think are needed, and we will try to include them in future releases.

![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png)


---

**WARNING!**: This is the *Old* source-code repository for MGFVisor3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/mgfvisor3/) located at https://sourceforge.net/p/lp-csic-uab/mgfvisor3/**  

---  
  
