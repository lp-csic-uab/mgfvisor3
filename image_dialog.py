# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun  6 2014)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import os

###########################################################################
## Class ImageDialog
###########################################################################

class ImageDialog ( wx.Dialog ):
    
    def __init__( self, parent, image_filen, **kwargs ):
        self.file = image_filen
        self.default_dir = os.path.expanduser('~')
        self.wildcard = "PNG (*.png)|*.png|" \
                        "JPG (*.jpg)|*.jpg|"
        if not 'title' in kwargs:
            kwargs['title'] = image_filen
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, style = wx.DEFAULT_DIALOG_STYLE, **kwargs )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        bSizer1 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_bitmap1 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap(image_filen, wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.TRANSPARENT_WINDOW )
        #self.m_bitmap1.SetToolTipString(image_filen)
        bSizer1.Add( self.m_bitmap1, 1, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )
        
        bSizer2 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_button1 = wx.Button( self, wx.ID_OK )
        bSizer2.Add( self.m_button1, 0, wx.ALL, 5 )
        
        self.m_button2 = wx.Button( self, wx.ID_SAVEAS )
        bSizer2.Add( self.m_button2, 0, wx.ALL, 5 )
        self.Bind(wx.EVT_BUTTON, self.on_saveas, self.m_button2)
        
        bSizer1.Add( bSizer2, 0, wx.ALIGN_CENTER, 5 )
        
        
        self.SetSizer( bSizer1 )
        self.Layout()
        bSizer1.Fit( self )
        
        self.Centre( wx.BOTH )
    
    def on_saveas(self, evt):
        """
        Ask for a filename to save the image of ``self.m_bitmap1`` as a PNG or 
        a JPG file.
        """
        dlg = wx.FileDialog(None, "Save Image As...",
                            defaultDir=self.default_dir,
                            wildcard=self.wildcard,
                            style=wx.SAVE | wx.FD_OVERWRITE_PROMPT)
        # Save the image to the specified file and file-type:
        if dlg.ShowModal() == wx.ID_OK:
            self.file = dlg.GetPath()
            self.default_dir = os.path.dirname(self.file)
            ext = os.path.splitext(self.file)[1].upper()
            if not ext: #Get the extension from the current filter index:
                ext = '.PNG' if dlg.GetFilterIndex() == 0 else '.JPG'
                self.file += ext.lower()
            img_type = wx.BITMAP_TYPE_PNG if ext == '.PNG' else wx.BITMAP_TYPE_JPEG
            img = self.m_bitmap1.GetBitmap()
            img.SaveFile(self.file, img_type)

