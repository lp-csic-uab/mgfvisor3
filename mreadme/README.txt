Description

MgfVisor is a tool for viewing collections of spectra stored as MGF files.


Table Of Contents:
------------------
    Features
    Installation
        Requirements
        From Installer
        From Source
    Download
    Change-Log
    To-Do
    Contribute

 
Features
--------
    Reads MGF files which header tags include TITLE and SCANS tags.
    Spectrum viewer with ion labels.
    Diverse spectrum data collected (peak number, parent ion, rt time).
    Detection of peak pairs suggesting an aa residue.
    Estimate molecular ion and bn-1 transition
    Graph transitions.
    Detects graph with paths longer than two transitions.


Installation
------------
Tested in Windows XP 32 bits and Windows 7 64 bits.
Requirements

    Graphviz:  MgfVisor uses the Graphviz package to plot graphs (last version
    tested is 2.28.0):

1) From Installer

    Download MgfVisor_x.y_setup.exe windows installer from repository at the
      MgfVisor project download page.
    Get your Password for the installer.
    Double click on the installer and follow the Setup Wizard.
    Run MgfVisor by double-clicking on the MgfVisor short-cut in your desktop
      or from the START-PROGRAMS application folder created by the installer.

2) From Source

    Install Python and third party software indicated in Dependencies.
    Download MgfVisor source code from its Mercurial Source Repository.
    Download commons source code from its Mercurial Source Repository.
    Copy the folders in python site-packages or in another folder in the python path.
    Run MgfVisor by double-clicking on the mvisor.pyw module.

Source Dependencies:

    Python 2.7 (not tested with other versions)
    wxPython 2.8.11.0 - 2.8.11.2
    matplotlib 1.2.0
    pydot 1.0.28
    networkX 1.7
    commons (from LP CSIC/UAB Google Code repository)

Third-party program versions correspond to those used for the installer available
here. Lower versions have not been tested, although they may also be fine.

Download
---------
You can download the last version of MgfVisor here.

    After downloading the binary installer, you have to e-mail us at to get your
    free password and unlock the installation program.
    The password is not required to run the application from source code 

 
Change-Log
-----------

1.0.0 December 2012
- First version release.

To-Do
-------

- Take into account the monocharged parent ion to mark its position in the spectrum.

Contribute
----------
These programs are made to be useful. If you use them and don't work entirely
as you expect, let us know about features you think are needed, and we will try
to include them in future releases.