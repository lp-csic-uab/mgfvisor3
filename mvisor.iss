; For MgfVisor
; J.Abian 20 december 2012

#define MyAppName ReadIni(SourcePath + "minstall.ini", "Common", "name", "noname")
#define MyVersion ReadIni(SourcePath + "minstall.ini", "Common", "version", "0.0.0")
#define MyImageDir ReadIni(SourcePath + "minstall.ini", "Common", "imgdir", "")
#define MyWizardDir ReadIni(SourcePath + "minstall.ini","Common", "imgdir", "")
#define MyBigIcon ReadIni(SourcePath + "minstall.ini", "Common", "big_icon", "")
#define MySmallIcon ReadIni(SourcePath + "minstall.ini", "Inno","small_icon", "")
#define MyWizardImage ReadIni(SourcePath + "minstall.ini","Inno","wzimg","")
#define MyWizardSecondImage ReadIni(SourcePath + "minstall.ini","Inno","wzsimg","")
#define MyPswd ReadIni(SourcePath + "minstall.ini", "Inno", "pswd", "lpcsicuab")
#define MyInstall "mreadme\INSTALL.txt"
#define MyComment "Visualizes mgf files"

[Setup]
AppName={#MyAppName}
;data for unins000.dat file
AppId={#MyAppName} {#MyVersion}
;appears in the first page of the installer
;AppVerName={cm:NameAndVersion,KimBlast,{cm:Myvers}}
;appears in the support info for add/remove programs
AppVersion={#MyVersion}
AppPublisher=Joaquin Abian
DefaultDirName={pf}\{#MyAppName}_{#MyVersion}
UsePreviousAppDir=no
DefaultGroupName=KimKaos
Compression=lzma/max
AllowNoIcons=yes
AllowRootDirectory=yes
UsePreviousLanguage=no
UninstallDisplayIcon={#MyImageDir}\{#MySmallIcon}
OutputBaseFilename={#MyAppName}_{#MyVersion}_setup
OutputDir=installer
InfoAfterFile={#MyInstall}
LicenseFile="LICENCE.txt"
Password={#MyPswd}
WizardImageFile={#MyImageDir}\{#MyWizardImage}
#ifdef MyWizardSecondImage
  WizardSmallImageFile={#MyWizardDir}\{#MyWizardSecondImage}
#endif
AppCopyright=Pending 2012 Joaquin Abian
;appears in properties "version del archivo" and "version del producto"
;of the Setup.exe program in the "Version" page
;and also in the info when Setup.exe is selected with the cursor and where it adds a zero
VersionInfoVersion={#MyVersion}
SetupIconFile={#MyImageDir}\{#MyBigIcon}

[Files]
Source: "mdist\test\*"; DestDir: "{app}\test"
Source: "mdist\*"; DestDir: "{app}"
Source: "mdist\images\green.png"; DestDir: "{app}\images"
Source: "mdist\images\on.png"; DestDir: "{app}\images"
Source: "mdist\images\off.png"; DestDir: "{app}\images"
Source: {#MyImageDir}\{#MySmallIcon}; DestDir:{app}
#if FileExists(SourcePath + "mdist\mpl-data\matplotlibrc")
  Source: "mdist\mpl-data\*"; DestDir: "{app}\mpl-data"
  Source: "mdist\mpl-data\images\*"; DestDir: "{app}\mpl-data\images"
#endif

#if FileExists(SourcePath + "mdist\doc\README.html")
  Source: "mdist\doc\*"; DestDir: "{app}\doc"
#endif

[Tasks]
;CreateDesktopIcon is defined in Default.isl
Name: desktopicon; Description: "{cm:CreateDesktopIcon}"

[Icons]
Name: "{group}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe" ; IconFilename:{app}\{#MySmallIcon};WorkingDir: "{app}"; Comment: {#MyComment}
Name: "{userdesktop}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe"; WorkingDir: "{app}"; Tasks: desktopicon
