import wx
import os
import numpy
import pydot
import networkx as nx
from commons.warns import tell
from commons.mass import mass_aa, mass_group
from visor_main import Visor
from mvisor_data_panel import DataPanel
from visor_images import MICON
from image_dialog import ImageDialog

VISOR_TITLE = "  Visor MGF 1.1"

def get_consts():
    masses = mass_aa.copy()
    for aa in 'LIX':
        masses.pop(aa)
    water = mass_group['H2O'][1]
    m_aas = {value[1]: key for key, value in masses.items()}
    m_bn = {value[1] + water: key for key, value in masses.items()}

    return m_aas, m_bn

MASS_AAS, MASS_Bn = get_consts()

class MgfVisor(Visor):
    """"""
    def __init__(self, *args, **kwds):
        kwds['data_panel'] = DataPanel
        kwds['title'] = VISOR_TITLE
        kwds['icon'] = MICON

        Visor.__init__(self, *args, **kwds)
        #
        # to overwrite Visor attributes
        self.wildcard = "MGF (*.mgf)|*.mgf|" \
                        "Text (*.txt)|*.txt|"
        self.ext = '.mgf'
        #
        self.jumps = []
        self.precursor = None
        self.masses = None
        self.mol_ion = None
        self.charge = None
        self.major_ions = None
        self.graph = None
        #
        user_dir = os.path.expanduser('~')
        self.user_dir = os.path.join(user_dir, 'visor_data')
        if not os.path.exists(self.user_dir):
            os.mkdir(self.user_dir)

        self.Bind(wx.EVT_BUTTON, self.on_graph, self.data_pan.bt_graph)
        self.Bind(wx.EVT_CHECKBOX, self.on_monitor, self.data_pan.ckbx_monitor)
    #
    def get_data(self):
        """Reads data from mgf file.

        It parses the mgf file and store data in a dictionary of dictionaries
        Currently accepts any mgf file bearing TITLE and SCANS headers.
        This include lpcsicuab custom mgf files:

            BEGIN IONS
            TITLE=INN_INF1_T_DIG8H_10%_121003,Scan:2452,MS:2,Rt:46.0140
            PEPMASS=491.60955811
            SCANS=2452
            RTINSECONDS=46.0140
            140.939666748 127.458374023
            143.081161499 94.2878265381
            ...........................
            END IONS

        All header tags and corresponding data are stored in the spectrum
        dictionaries as key, values.

        """
        self.data = {}
        with open(self.file) as handle:
            data = handle.read()
        #
        data = data.split('BEGIN IONS')
        for item in data:
            if not item:
                continue
            lines = item.strip().split('\n')

            ilines = iter(lines)
            partial = {}
            spectrum = []
            line = next(ilines)
            #
            # read header lines
            while line[0].isalpha():
                key, value = line.split('=', 1)
                partial[key] = value
                line = next(ilines)
            #
            # read mass, intensity lines
            while line != 'END IONS':
                mass, intensity = line.split()
                spectrum.append((float(mass), float(intensity)))
                line = next(ilines)

            partial['spectrum'] = spectrum
            mgf = partial.get('TITLE', 'None')
            mgf = mgf.split(',')[0]
            scan = partial.get('SCANS', 'None')
            self.data['%s.%s' % (mgf, scan)] = partial
    #
    def get_spectrum(self):
        """Reads spectrum from data dictionary.

        Sets self.intensities, self.masses, self.precursor

        """
        self.jumps = []

        data = self.data[self.spectrum_file]
        masses, intensities = zip(*data['spectrum'])

        self.intensities = numpy.array(intensities, dtype='float')
        self.masses = numpy.array(masses, dtype='float')
        self.precursor = float(data['PEPMASS'])
    #
    def get_parameters(self):
        """Calculates spectrum parameters """
        data = self.data[self.spectrum_file]
        data['peak_number'] = len(self.masses)
    #
    def identify_ions(self):
        """Core function to annotate and monitor spectrum and transition data.

        Call functions to:
            set collection of major ions (with deisotoping)
            annotate major ions in the spectrum view
            determine transitions (jumps corresponding to an aa residue)
            monitor for long paths in the graph of transitions

        """
        self.init_major_ions()
        self.calculate_mass_charge()
        self.set_annotations()
        self.calculate_transitions()
        if self.data_pan.ckbx_monitor.IsChecked():
            self.monitor()
    #
    def init_major_ions(self):
        """Find n major ions sorted by mass.

        n is hardcoded to 35
        Deisotopes effectively up to mz 1000 for +1 (isotope < 50% parent)
        At higher mass, isotope < 75% parent. this holds maybe up to mz 2000.

        Sets self.major_ions

        """
        data = self.data[self.spectrum_file]['spectrum']
        spectrum = sorted(data, key=lambda x: x[1], reverse=True)
        major_ions = sorted(spectrum[:35])
        last_mass = 0
        last_intensity = 0
        self.major_ions = []
        for mass, intensity in major_ions:
            if (mass - last_mass) < 2:
                if (mass < 1000) and (2 * intensity < last_intensity):
                    pass
                elif (mass > 1000) and (1.5 * intensity < last_intensity):
                    pass
                else:
                    self.major_ions.append((mass, intensity))
            else:
                self.major_ions.append((mass, intensity))
            last_mass = mass
            last_intensity = intensity
    #
    def calculate_mass_charge(self):
        """Calculates MH+ mass and charge from precursor data.

        Assumes:
         MH+ must be higher than any ion in a spectrum.
         Precursors have charge 2 or 3.

        """
        proton = mass_group['H'][1]
        self.mol_ion = 0
        self.charge = 0
        for n in (2, 3):
            mol_ion = (self.precursor * n) - (n - 1) * proton
            if mol_ion > max(self.major_ions)[0]:
                self.mol_ion = mol_ion
                self.charge = n
                break

        data = self.data[self.spectrum_file]
        data['mol_ion'] = self.mol_ion
        data['charge'] = self.charge
    #
    def set_annotations(self):
        """Identify higher peak in each section of the scan for annotation.

        Current approach takes the n higher ions sorted by mass and annotates
        them only if distance from the previous one is higher than a minimum.
        If the distance is lower but the next, higher mass ion, is of higher
        intensity, this second ions replaces the previous annotation.

        """
        ion_number = len(self.masses)
        color = 'black'
        # initializes self.colors
        self.colors = ['k'] * ion_number
        self.annotations = []
        last_mass = 0
        last_intensity = 0
        for mass, intensity in self.major_ions:
            if (mass - last_mass) > 20:
                last_intensity = intensity
                self.annotations.append(('%.1f' % mass, color, mass, intensity))
                last_mass = mass
            elif intensity > last_intensity:
                self.annotations[-1] = ('%.1f' % mass, color, mass, intensity)
                last_intensity = intensity
                last_mass = mass
    #
    # noinspection PyAssignmentToLoopOrWithParameter
    def calculate_transitions(self):
        """Calculate mass differences corresponding to aa residues

        Calculates y, b and also bn-1 transitions

        """
        self.jumps = []
        precursor = self.precursor
        width = self.precision
        mass_windows = [(mass - width, mass, mass + width) for mass in MASS_AAS]
        spectrum = self.major_ions + [(self.mol_ion, 0)]
        for mass, _ in spectrum:
            if mass > precursor:
                for s_mass, _ in spectrum:
                    jump = s_mass - mass
                    for lower_mass, aa_mass, higher_mass in mass_windows:
                        if lower_mass < jump <= higher_mass:
                            self.jumps.append((mass, s_mass, MASS_AAS[aa_mass]))
                            # break
        # get bn-1
        bn_windows = [(mass - width, mass, mass + width) for mass in MASS_Bn]
        for mass, _ in spectrum[-5:]:
            jump = self.mol_ion - mass
            for lower_mass, aa_mass, higher_mass in bn_windows:
                if lower_mass < jump <= higher_mass:
                    self.jumps.append((mass, self.mol_ion,
                                       MASS_Bn[aa_mass] + '*'))
                    # break

        jumps = ['%.1f -> %.1f : %s' % (m, sm, aa) for m, sm, aa in self.jumps]
        self.data[self.spectrum_file]['matches'] = '\n' + '\n'.join(jumps)
    #
    def fill_info(self):
        """Fill boxes with data"""
        data = self.data[self.spectrum_file]
        keys = [key for key in data.keys() if key.isupper()]

        self.write_box(data, 'tc_gi', False, *keys)
        self.write_box(data, 'tc_ss', False, 'peak_number', 'mol_ion', 'charge')
        self.write_box(data, 'tc_match', False, 'matches')
    #
    # noinspection PyUnusedLocal
    def on_monitor(self, evt):
        """Set monitoring on and off"""
        if self.data_pan.ckbx_monitor.IsChecked():
            if self.spec_pan.precision.cbx_label.IsChecked():
                self.data_pan.bmp_light.SetBitmap(self.data_pan.on)
            else:
                tell('Must also check "label" to allow monitoring')
                self.data_pan.ckbx_monitor.SetValue(False)
        else:
            self.data_pan.bmp_light.SetBitmap(self.data_pan.off)
    #
    # noinspection PyUnusedLocal
    def on_graph(self, evt):
        """Makes and shows graph of jumps (transitions)"""
        if not self.jumps:
            self.identify_ions()
        self.make_graph()
        self.show_graph()
    #
    def make_graph(self):
        """Build graph of transitions."""
        self.graph = pydot.Dot()
        for s, d, l in self.jumps:
            edge = pydot.Edge('%.2f' % s, '%.2f' % d, label=l, weight=1)
            self.graph.add_edge(edge)
    #
    def show_graph(self):
        """Draw graph in default image visor.

        Uses pydot so it requires graphviz installed.
        Networkx can use matplotlib but I did not find and easy way of
        labelling edges.
        Uses os.startfile to show the image, after saving. Other options using
        PIL are not good:
            im = pil.open(out)
            #option1 (opens a mpl window with non convenient size)
            plt.imshow(im)
            plt.show()

            #option 2 (freezes the application)
            im.show()

        """

        out = os.path.join(self.user_dir, self.spectrum_file + '.png')
        try:
            # noinspection PyUnresolvedReferences
            self.graph.write_png(out)
            # os.startfile(out)
            ImageDialog(self, out).Show()
        except pydot.InvocationException:
            tell('It seems graphviz is not installed in this computer\nInstall'
                 ' from http://www.graphviz.org/')
    #
    # noinspection PyPep8Naming
    def monitor(self):
        """Detect graphs with paths of more than 2 edges

        Uses a shortest path algorithm from NetworkX.
        Needs a graph with weights defined.

        """
        self.make_graph()
        nxg = nx.drawing.nx_pydot.from_pydot(self.graph)

        H = nx.DiGraph(nxg)
        for u, v in H.edges():
            H[u][v]['weight'] *= -1

        light = False
        for u, v in nxg.edges():
            vals, distances = nx.bellman_ford_predecessor_and_distance(H, u)
            if min(distances.values()) < -2:
                light = True
                break

        if light:
            self.data_pan.bmp_light.SetBitmap(self.data_pan.green)
        else:
            self.data_pan.bmp_light.SetBitmap(self.data_pan.on)
#
#
#
if __name__ == '__main__':

    app = wx.App()
    mvisor = MgfVisor(None)
    mvisor.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
