#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
visor_main (visor_09)
15 march 2011
"""
#
#
import os
import wx
import numpy
#
from matplotlib import rcParams as rc
#
from visor_spectrum_panel import SpectrumPanel
from commons.iconic import Iconic
from commons.warns import tell
#
rc['figure.subplot.left'] = 0.07
rc['figure.subplot.right'] = 0.96
rc['xtick.labelsize'] = 'x-small'
rc['ytick.labelsize'] = 'x-small'
rc['axes.labelsize'] = 'small'
rc['xtick.direction'] = 'out'
rc['font.size'] = 11.0
#
#
VISOR_TITLE = "  Generic Visor"
#
#
class MyFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window

    # noinspection PyMethodOverriding
    def OnDropFiles(self, x, y, filenames):
        self.window.notify(filenames)
#
#
# noinspection PyUnusedLocal
class Visor(wx.Frame, Iconic):
    def __init__(self, *args, **kwds):
        MyDataPanel = kwds.pop('data_panel')
        MyIcon = kwds.pop('icon')
        TITLE = kwds.pop('title', VISOR_TITLE)
        wx.Frame.__init__(self, *args, **kwds)
        Iconic.__init__(self, icon=MyIcon)

        self.file = os.path.join(os.path.dirname(__file__), 'test/test.mgf')

        self.spectrum_file = ""
        self.spectrum_files = []
        self.wildcard = None
        self.ext = None
        self.masses = None
        self.intensities = None
        self.data = None
        self.annotations = None
        self.default_dir = ""
        self.colors = 'black'
        self.precision = None

        self.sp = wx.SplitterWindow(self)
        self.spec_pan = SpectrumPanel(self.sp)
        self.data_pan = MyDataPanel(self.sp)

        dt1 = MyFileDropTarget(self)
        dt2 = MyFileDropTarget(self)
        self.data_pan.tc_file.SetDropTarget(dt1)
        self.data_pan.lbx_spectra.SetDropTarget(dt2)

        self.data_pan.tc_file.SetToolTip(
                                        "enter path for data file either\n"
                                        "  - manually\n"
                                        "  - pressing 'get'\n"
                                        "  - drag&dropping file")
        self.data_pan.spb_search.SetToolTip(
                                    "press spinbox arrows to search the string\n"
                                    "below or above the current position")
        self.data_pan.tc_search.SetToolTip(
                                    "enter any string and press arrows to search")

        # noinspection PyArgumentList

        self.sp.SetSashGravity(1.0)   # Allow only the top panel to expand
        self.sp.SetMinimumPaneSize(10)
        self.SetSize((850, 750))
        self.sp.SplitHorizontally(self.spec_pan, self.data_pan, sashPosition=0)

        self.Layout()

        self.SetTitle(TITLE)

        self.Bind(wx.EVT_BUTTON, self.on_file, self.data_pan.bt_file)
        self.Bind(wx.EVT_LISTBOX, self.on_spectrum, self.data_pan.lbx_spectra)
        self.Bind(wx.EVT_SPIN_UP, self.on_search_up, self.data_pan.spb_search)
        self.Bind(wx.EVT_SPIN_DOWN, self.on_search_down, self.data_pan.spb_search)

        self.init()
    #
    def init(self):
        """to be implemented in child

        Is this method actually required ?
        """
        self.init_view()
    #
    def notify(self, files):
        """Informs when a drag and drop occurs."""
        if files[0].endswith(self.ext):
            self.file = files[0]
            self.default_dir = os.path.dirname(self.file)
            self.init_view()
        else:
            tell("Extension of file not valid.\nMust be %s" % self.ext)
    #
    def init_view(self):
        """show data from first spectrum_file after loading a db"""
        self.get_files()
        self.spectrum_file = self.data_pan.lbx_spectra.GetString(0)
        self.get_draw_fill_all()
    #
    def on_spectrum(self, evt):
        """show data after selecting a spectrum_file in listbox"""
        new_file = self.data_pan.lbx_spectra.GetStringSelection()

        if not new_file == self.spectrum_file:
            self.spectrum_file = new_file
            self.get_draw_fill_all()
    #
    def get_draw_fill_all(self):
        """"""
        try:
            self.precision = float(
                self.spec_pan.precision.tc_precision.GetValue())
        except ValueError:
            self.precision = 0.3
            self.spec_pan.precision.tc_precision.SetValue(str(self.precision))

        self.get_spectrum()
        self.get_parameters()
        self.draw_spectrum()
        self.fill_info()
    #
    def draw_spectrum(self):
        """"""
        zeros = numpy.zeros(len(self.masses))
        self.spec_pan.clean()

        if self.spec_pan.precision.cbx_label.IsChecked():
            self.identify_ions()
            self.spec_pan.axes.vlines(self.masses, zeros, self.intensities,
                                      color=self.colors, label='False')
            self.mark_ion()
        else:
            self.spec_pan.axes.vlines(self.masses, zeros, self.intensities,
                                      color='black', label='False')
        # print 'go and draw'
        self.spec_pan.draw()
    #
    def on_file(self, evt):
        """"""
        wildcard = self.wildcard + \
                   "ALL (*.*)|*.*"
        dlg = wx.FileDialog(None, "Select File",
                            defaultDir=self.default_dir,
                            wildcard=wildcard,
                            style=wx.FD_OPEN)

        if dlg.ShowModal() == wx.ID_OK:
            self.file = dlg.GetPath()
            self.default_dir = os.path.dirname(self.file)

        self.get_files()
    #
    def get_files(self):
        """"""
        self.data_pan.tc_file.SetValue(self.file)
        self.get_data()
        self.spectrum_files = sorted(self.data.keys())
        self.data_pan.lbx_spectra.Set(self.spectrum_files)
    #
    def fill_info(self):
        """to be implemented on child"""
        pass
    #
    def get_data(self):
        """to be implemented on child"""
        pass
    #
    def get_spectrum(self):
        """to be implemented on child"""
        pass
    #
    def get_parameters(self):
        """to be implemented on child"""
        pass
    #
    def identify_ions(self):
        """to be implemented on child"""
        pass
    #
    def mark_ion(self):
        """Annotates ion type in spectrum.

        use axes.annotate(s, xy, xy_text, rotation).
        axes.text(s,x,y) do not work well with the zoom

        """
        annotate = self.spec_pan.axes.annotate

        for note, color, mass, intensity in self.annotations:
            annotate(note, xy=(mass, intensity),
                           color=color, xytext=None,
                           va='bottom', rotation='vertical')
    #
    def on_search_down(self, evt):
        """"""
        listbox = self.data_pan.lbx_spectra
        query = self.data_pan.tc_search.GetValue()
        choice_pos = listbox.GetSelection()
        for item in self.spectrum_files[choice_pos + 1:]:
            if query in item:
                choice_pos = listbox.FindString(item)
                listbox.SetSelection(choice_pos)
                self.on_spectrum(None)
                break
    #
    def on_search_up(self, evt):
        """"""
        listbox = self.data_pan.lbx_spectra
        query = self.data_pan.tc_search.GetValue()
        choice_pos = listbox.GetSelection()
        for item in reversed(self.spectrum_files[0:choice_pos]):
            if query in item:
                choice_pos = listbox.FindString(item)
                listbox.SetSelection(choice_pos)
                self.on_spectrum(None)
                break

    # noinspection PyUnresolvedReferences
    def write_box(self, box_data, box_name, color, *args):
        """Fill the box <box_name> with the corresponding info.
        """
        box = getattr(self.data_pan, box_name)
        #
        if box_data:
            text = ''
            for item in args:
                try:
                    text += '%s = %s\n' % (item, box_data[item])
                except KeyError:
                    if item:
                        text += '%s = %s\n' % (item, 'n/a')

            if color:
                sequence = box_data.get('modified_sequence', None)
                if (self.peptide_consensus == sequence) or \
                   (self.peptide_from_ascore == sequence):
                    box.SetForegroundColour('black')
                else:
                    box.SetForegroundColour('grey')

            box.SetValue(text)
        else:
            box.SetValue("")
            #
#
#
#
if __name__ == '__main__':

    from visor_images import MICON

    app = wx.App()
    panel = wx.Panel()
    visor = Visor(None, data_panel=panel, icon=MICON)
    visor.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
