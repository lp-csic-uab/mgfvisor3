#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
"""
jvisor_spectrum_panel (visor_07)
25 julio 2010
"""
#
import wx
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
#
#
class ControlCut(wx.Panel):
    def __init__(self, parent, position):
        wx.Panel.__init__(self, parent, pos=position, size=(210, 40))
        psx, psy = 5, 3
        szy = 21
        self.lb_zoom = wx.StaticText(self, -1, 'zoom', pos=(psx, psy+3),
                                     size=(35, szy))
        self.zoom_init = wx.TextCtrl(self, -1, pos=(psx+35, psy),  
                                     size=(45, szy), style=wx.TE_READONLY)
        self.lb_sep = wx.StaticText(self, -1, '-', pos=(psx+85, psy+3),
                                    size=(5, szy))
        self.zoom_end = wx.TextCtrl(self, -1, pos=(psx+92, psy), size=(45, szy),
                                    style=wx.TE_READONLY)
        self.cbx_freeze = wx.CheckBox(self, -1, 'freeze', pos=(psx+143, psy),
                                      size=(75, szy))
        #
        self.zoom_init.SetToolTip("min m/z value visualized")
        self.zoom_end.SetToolTip("max m/z value visualized")
        self.cbx_freeze.SetToolTip("freeze the x-axes for the next spectra")
        # self.SetBackgroundColour("light gray")
#
#
class ControlPrecision(wx.Panel):
    def __init__(self, parent, position):
        wx.Panel.__init__(self, parent, pos=position, size=(170, 40))
        psx, psy = 5, 3
        szy = 21
        self.label = wx.StaticText(self, label='precision', pos=(psx, psy+3),
                                   size=(55, szy))
        self.tc_precision = wx.TextCtrl(self, pos=(psx+55, psy), size=(45, szy))
        self.cbx_label = wx.CheckBox(self, label='label', pos=(psx+115, psy),
                                     size=(80, szy))
        #
        self.tc_precision.SetToolTip("max m/z error for fragment assignment")
        self.cbx_label.SetToolTip("show fragment assignments in spectrum")
        # self.SetBackgroundColour("light gray")
#
#
# noinspection PyUnusedLocal
class SpectrumPanel(wx.Panel):
    # noinspection PyArgumentList
    def __init__(self, parent, xlabel='m/z', ylabel='Intensity', choices=None):
        wx.Panel.__init__(self, parent)
        #
        self.parent = parent
        self.choice = choices if choices else []
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.lim = ()
        #
        self.toolbar = None
        self.control = None
        self.mass = None
        self.precision = None
        # self.SetBackgroundColour("white")
        #
        self.figure = Figure()
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.canvas.SetMinSize((100, 100))
        #
        toolbar = self.add_toolbar()
        #
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.ALIGN_CENTER | wx.EXPAND)
        self.sizer.Add(toolbar, 0, wx.ALIGN_LEFT)
        self.canvas.mpl_connect('draw_event', self.on_zoom)
        self.canvas.mpl_connect('motion_notify_event', self.on_motion)
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.sizer.Fit(self)
        self.clean()
    #
    def add_toolbar(self):
        """"""
        self.toolbar = NavigationToolbar2Wx(self.canvas)
        #
        self.toolbar.DeleteToolByPos(6)
        self.toolbar.DeleteToolByPos(5)
        #
        self.toolbar.SetToolBitmapSize(wx.Size(24, 25))
        self.toolbar.SetMinSize((250, 31))
        self.toolbar.Realize()
        self.toolbar.Update()
        #
        mass_txt = wx.StaticText(self, label='m/z', pos=(230, 7),
                                 size=(25, 17))
        self.mass = wx.TextCtrl(self, pos=(260, 4), size=(55, 22),
                                style=wx.TE_READONLY)
        self.mass.SetToolTip("m/z value at cursor position")
        self.control = ControlCut(self, position=(340, 2))
        self.precision = ControlPrecision(self, position=(560, 2))
#         #
#         color = self.toolbar.GetBackgroundColour()
#         mass_txt.SetBackgroundColour(color)
#         self.control.SetBackgroundColour(color)
#         self.precision.SetBackgroundColour(color)
#         #
        tb_sizer = wx.BoxSizer(wx.HORIZONTAL)
        tb_sizer.Add(self.toolbar, 0, wx.ALIGN_CENTER, 5)
        tb_sizer.AddSpacer(15)
        tb_sizer.Add(mass_txt, 0, wx.ALIGN_CENTER, 5)
        tb_sizer.Add(self.mass, 0, wx.ALIGN_CENTER, 5)
        tb_sizer.AddSpacer(25)
        tb_sizer.Add(self.control, 0, wx.ALIGN_CENTER, 5)
        tb_sizer.AddSpacer(15)
        tb_sizer.Add(self.precision, 0, wx.ALIGN_CENTER, 5)
        tb_sizer.AddSpacer(15)
        self.tb_sizer = tb_sizer
        #
        self.Bind(wx.EVT_CHECKBOX, self.on_freeze, self.control.cbx_freeze)
        #
        return tb_sizer
    #
    def clean(self):
        """"""
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)
    #
    def draw(self):
        """draw the canvas"""
        self.axes.set_xlabel(self.xlabel)
        self.axes.set_ylabel(self.ylabel)
        if self.lim:
            self.axes.set_xlim(self.lim)

        self.canvas.draw()
    #
    def on_zoom(self, evt):
        """each draw"""
        if not self.control.cbx_freeze.IsChecked():
            xlim = self.axes.get_xlim()    
            self.control.zoom_init.SetValue('%i' % xlim[0])
            self.control.zoom_end.SetValue('%i' % xlim[1])
    #
    def on_freeze(self, evt):
        """"""
        if self.control.cbx_freeze.IsChecked():
            self.lim = self.axes.get_xlim()
            self.control.zoom_init.SetValue('%i' % self.lim[0])
            self.control.zoom_end.SetValue('%i' % self.lim[1])
        else:
            self.lim = ()
    #
    def on_motion(self, evt):
        if evt.inaxes:
            xpos = evt.xdata
            self.mass.SetValue(' %0.1f' % xpos)
        

if __name__ == '__main__':

    class TestFrame(wx.Frame):
        def __init__(self, *args, **kwargs):
            wx.Frame.__init__(self, *args, **kwargs)
            self.panel = SpectrumPanel(self)
            self.Fit()
    #        
    app = wx.PySimpleApp()
    fr = TestFrame(None)
    fr.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
